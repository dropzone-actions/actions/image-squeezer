# Dropzone Action Info
# Name:                     Image Squeezer
# Description:              Takes one or more images and converts them into JPG while compressing them losslessly with ImageOptim
# Handles:                  Files
# Creator:                  Kolja Nolte
# URL:                      https://www.kolja-nolte.com
# Events:                   Dragged
# SkipConfig:               No
# RunsSandboxed:            Yes
# Version:                  1.0.0
# MinDropzoneVersion:       3.5

# Import required modules
import os
import hashlib
import time


# Tell Dropzone that this function is being executed via drag & drop
def dragged():
    # Change the variable name for better readability (`items` stores the dropped file paths)
    images = items

    # Displays the label of the current process
    dz.begin('Start image conversion...')

    # Enable percentage
    dz.determinate(True)

    # Breakdown how many percent of 100% is each image
    percentage_per_iteration = 100 / len(images) * 100 / 100

    # Start percentage display
    converted_images = 0

    # Loop through each image
    for image_path in images:

        # Check whether the file actually exists
        if not os.path.isfile(image_path):
            # If not, display a failure message and cancel the script
            dz.error('An error has occurred',
                     'The dropped image ' + image_path + ' is not a valid file and can\'t be converted.')

            # Kill action
            exit()

        # Current total percentage
        dz.percent(0 + percentage_per_iteration)

        # UNIX timestamps are helpful if you need an always random (integer) number
        timestamp = time.time()

        # Convert the timestamp from int to string
        timestamp = str(timestamp)

        # Encode the timestamp (this is needed for md5 hashes)
        timestamp = timestamp.encode('utf-8')

        # Finally, turn the timestamp into an md5 hash
        random_file_name = hashlib.md5(timestamp).hexdigest()

        # Create a variable for the new file name and shorten it to 10 characters
        new_file_name = random_file_name[0:10]

        # Find the directory the dropped image(s) are located in
        current_directory = os.path.dirname(image_path)

        # Generate the path to the new file with its random filename
        new_image_path = current_directory + '/' + new_file_name + '.jpg'

        # Use `convert` to resize the image to 1280px on the longest side and reduce the quality to 70%
        os.system('/usr/local/bin/convert -resize 1280x -quality 70 "' + image_path + '" "' + new_image_path + '"')

        # The default path to ImageOptim (if installed)
        image_optim_path = '/Applications/ImageOptim.app/Contents/MacOS/ImageOptim'

        # Check if the image actually exists and is a valid file
        if not os.path.isfile(image_path):
            dz.error(
                'An error has occurred',
                'One or more specified images could not be found or is not a valid file.'
            )

            # Kill action
            exit()

        if not os.path.isfile(image_optim_path):
            # Display alert message but continue script
            dz.alert(
                'ImageOptim not found',
                'The compression tool ImageOptim seems to be not installed. Go to https://imageoptim.com for better compression.'
            )

        else:
            # Losslessly compress the new file via ImageOptim
            os.system(image_optim_path + '"' + new_image_path + '"')

        # Add 1 to successfully converted image for the output display
        converted_images += 1

    # Stops the percentage
    dz.text('')

    # Default text
    text = 0

    # Text if only one image was dropped
    if converted_images == 1:
        text = '1 image has been converted'

    # Text if more than one image was dropped
    elif converted_images > 1:
        text = str(len(images)) + ' images have been converted.'

    # Display the success notification
    dz.finish(text)
