# Image Squeezer

> Latest version: 1.0.0

Image Squeezer accepts one or multiple images of any format, converts them into JPG, reduces the quality to 70%, resizes the image to 1280px on the longest side and - if available - compresses it once more with [ImageOptim](https://imageoptim.com/).

## Installation

1. Use `git clone <URL>` or [download](https://gitlab.com/dropzone-actions/actions/image-squeezer/-/archive/master/image-squeezer-master.zip) all files as an .zip archive.
2. Find the file with the extension `.dzbundle`.
3. Move this file to `~/Library/Application Support/Dropzone/Actions`.
4. Open Dropzone, click on the arrow in the top bar and drag and drop your action into *Add to Grid*.

## Usage

Simply drag and drop one or more images onto the action icon in Dropzone's dropdown menu. The converted and compressed files will then appear with a random name in the directory where the original image is stored.

## Modules Used
* `os`
* `hashlib`
* `time`

**Note**: All libraries listed come with macOS. You don't have to install any of these.